# Lugaru Blender plugins

This repository holds Blender plugins to handle the assets of the open source
game [Lugaru](https://osslugaru.gitlab.io).

The game uses custom binary formats for various resources, which can be imported
into Blender and exporter back to the custom binary formats using those scripts.

## Supported formats

- [Animation files](https://gitlab.com/osslugaru/lugaru/tree/master/Data/Animations),
  with `lugaru_anim_*.py`
- [Map files](https://gitlab.com/osslugaru/lugaru/tree/master/Data/Maps), with
  `lugaru_map_*.py`
- [Skeleton files](https://gitlab.com/osslugaru/lugaru/tree/master/Data/Skeleton),
  with `lugaru_skel_*.py`
- [Model files](https://gitlab.com/osslugaru/lugaru/tree/master/Data/Models)
  (`*.solid`), with `solid_*.py`

## Installation

Import the plugins into Blender via the User Preferences -> Add-ons.
